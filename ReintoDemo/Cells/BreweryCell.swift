//
//  BreweryCell.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class BreweryCell: UITableViewCell {
    
    let cityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        label.textColor = .gray
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        return label
    }()
    
    lazy var openButton: ActionButton = {
        let button = ActionButton(title: "↪︎")
        button.pressed = { [weak self] in
            self?.buttonPressed()
        }
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [cityLabel, titleLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    var buttonPressed: () -> Void = {}

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addOpenButton()
        setStackView()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setStackView() {
        contentView.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15).isActive = true
        stackView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15).isActive = true
        stackView.rightAnchor.constraint(equalTo: openButton.leftAnchor, constant: -20).isActive = true
    }
    
    private func addOpenButton() {
        contentView.addSubview(openButton)
        openButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15).isActive = true
        openButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        openButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        openButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
}
