//
//  LoggingService.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

class LoggingService {
    
    struct Response: Decodable {
        let userId: Int
    }
    
    init() {}
    
    func loginUser(_ username: String, password: String, completion: @escaping (Int?, Bool?) -> Void) {
        
        let parameters = ["login": username,
                          "password": password]
        
        guard let url = URL(string: APIURL.login) else {
            fatalError("Invalid URL: \(APIURL.login)")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "post"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                print(error ?? "Error")
                DispatchQueue.main.async {
                    completion(nil, nil)
                }
                return
            }
            do {
                let id = try JSONDecoder().decode(Response.self, from: data).userId
                DispatchQueue.main.async {
                    completion(id, true)
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }.resume()
        
    }
    
}
