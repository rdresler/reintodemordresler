//
//  BreweriesRepository.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

class BreweriesRepository {
    
    private let fileURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!.appendingPathComponent("Preferences", isDirectory: true).appendingPathComponent("breweries.json")
    
    private var breweryData = BreweryData()
    
    var breweries: [Brewery]
    
    var info: FetchInfo
    
    init() {
        do {
            if let data = try? Data(contentsOf: fileURL) {
                breweryData = try JSONDecoder().decode(BreweryData.self, from: data)
            }
        } catch {
            print(error)
        }
        breweries = breweryData.breweries
        info = breweryData.info
    }

    func save(_ breweries: [Brewery]) {
        
        self.breweries += breweries
        
        if breweries.count < info.itemsPerRequest {
            info.reachedEndOfItems = true
        }
        
        info.offset += info.itemsPerRequest
        
        do {
            let encoded = try JSONEncoder().encode(BreweryData(info: info, breweries: self.breweries))
            try encoded.write(to: fileURL)
        } catch { print(error) }
    }
    
}
