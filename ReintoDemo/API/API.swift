//
//  API.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct APIURL {
    static let login = "https://www.reintodev.cz:4006/api/login"
    static let breweries = "https://www.reintodev.cz:4006/api/breweries?"
}
