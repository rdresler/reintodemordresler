//
//  UITableViewExtensions.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) {
        let reuseIdentifier = String(describing: T.self)
        register(T.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    func dequeue<T: UITableViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T else {
            fatalError("Could not deque cell with type \(T.self)")
        }
        return cell
    }
    
}
