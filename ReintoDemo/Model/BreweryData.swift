//
//  BreweryData.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 26/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct BreweryData: Codable {
    var info: FetchInfo = FetchInfo()
    var breweries: [Brewery] = []
}

struct FetchInfo: Codable {
    let itemsPerRequest: Int = 20
    var offset: Int = 0
    var reachedEndOfItems: Bool = false
}
