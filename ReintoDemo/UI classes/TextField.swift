//
//  TextField.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class TextField: UITextField {
    
    init(frame: CGRect = .zero, placeholder: String) {
        super.init(frame: frame)
        self.placeholder = placeholder
        backgroundColor = UIColor(named: "LightGray")
        font = UIFont.systemFont(ofSize: 17, weight: .medium)
        layer.cornerRadius = 15
        setPaddingTo(10)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
