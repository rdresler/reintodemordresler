//
//  LoginViewController.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let usernameTextField = TextField(placeholder: "Username")
    
    let passwordTextField: TextField = {
        let textField = TextField(placeholder: "Password")
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var loginButton: ActionButton = {
        let button = ActionButton(title: "Log in")
        button.pressed = { [weak self] in
            self?.login()
        }
        button.layer.cornerRadius = 20
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [usernameTextField, passwordTextField])
        stackView.spacing = 15
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setStackView()
        setLoginButton()
    }
    
    private func setStackView() {
        view.addSubview(stackView)
        stackView.heightAnchor.constraint(equalToConstant: 125).isActive = true
        stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 60).isActive = true
        stackView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 20).isActive = true
        stackView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -20).isActive = true
    }
    
    private func setLoginButton() {
        view.addSubview(loginButton)
        loginButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: 130).isActive = true
        loginButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 80).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    let loggingService = LoggingService()
    
    private func login() {
        loggingService.loginUser(usernameTextField.text!, password: passwordTextField.text!) { [weak self] userId, success in
        //loggingService.loginUser("test@test.cz", password: "test") { [weak self] userId, success in
            guard let self = self else { return }
            guard let newUserId = userId else {
                if success == false {
                    self.showAlert(title: "Invalid login", message: "Incorrect username or password")
                    return
                } else {
                    let action = UIAlertAction(title: "Retry", style: .default) { [weak self] _ in
                        self?.login()
                    }
                    self.showAlert(title: "User cannot be logged", actions: [action], dismiss: false)
                }
                return
            }
            UserRepository.shared.userId = newUserId
            self.present(BreweriesViewController(), animated: true)
        }
    }
    
}
