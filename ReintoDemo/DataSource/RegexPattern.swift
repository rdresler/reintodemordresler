//
//  Regex.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 26/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct RegexPattern {
    static let nameDescriptionCityPattern = """
    ([A-z0-9À-ž'\\-,.%&° ]*[à-ža-z0-9])([A-Z][A-z0-9À-ž'\\-,.%&° ]*)\\ \\(([A-z0-9À-ž'\\-,.%&° ]*)
    """
    static let nameDescriptionPattern = """
    ([A-z0-9À-ž'\\-,.%&° ]*[à-ža-z0-9])([A-Z][A-z0-9À-ž'\\-,.%&° ]*)
    """
    static let nameCityPattern = """
    ([A-z0-9À-ž'\\-,.%&° ]*)\\ \\(([A-z0-9À-ž'\\-,.%&° ]*)
    """
    static let dotPattern = """
    (.*?)([A-z0-9À-ž'\\-,&%° ]*+[^A-Z])*([.].*)
    """
}
