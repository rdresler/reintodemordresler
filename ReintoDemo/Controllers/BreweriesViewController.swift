//
//  BreweriesViewController.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class BreweriesViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        return tableView
    }()
    
    var breweries = [Brewery]()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setTableView()
        getData(true)
    }
    
    let breweriesProvider = BreweriesProvider()
    
    private func getData(_ get: Bool = false) {
        breweriesProvider.fetch(get) { [weak self] breweries in
            guard let self = self else { return }
            guard let breweries = breweries else {
                let action = UIAlertAction(title: "Retry", style: .default) { [weak self] _ in
                    self?.getData()
                }
                self.showAlert(title: "Data cannot be loaded", actions: [action], dismiss: false)
                return
            }
            
            self.breweries = breweries
            
            self.tableView.reloadData()
        }
    }
    
    private func setTableView() {
        tableView.register(BreweryCell.self)
        view.addSubview(tableView)
        tableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -25)
    }

}

extension BreweriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return breweries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(BreweryCell.self, for: indexPath)
        
        let brewery = breweries[indexPath.row]
        cell.titleLabel.text = brewery.name
        cell.cityLabel.text = brewery.city
        cell.descriptionLabel.text = brewery.opened ? brewery.description : "Currently closed"
        cell.descriptionLabel.textColor = brewery.opened ? .lightGray : UIColor(named: "Red")!
        
        cell.buttonPressed = { [weak self] in
            guard let self = self else { return }
            guard let url = URL(string: self.breweries[indexPath.row].url) else {
                self.showAlert(title: "URL cannot be opened", message: "Try it later")
                return
            }
            self.present(BreweryPreviewViewController.instantiate(breweryUrl: url), animated: true)
        }
        
        if indexPath.row == breweries.count - 1 {
            getData()
        }
        
        return cell
    }
    
}
