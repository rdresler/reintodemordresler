//
//  ActionButton.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class ActionButton: UIButton {
    
    var pressed: () -> Void = {}

    init(frame: CGRect = .zero, title: String? = nil) {
        super.init(frame: frame)
        backgroundColor = UIColor(named: "Gray")
        setTitleColor(.black, for: .normal)
        setTitle(title, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func buttonPressed(_ sender: ActionButton) {
        pressed()
    }
    
}
