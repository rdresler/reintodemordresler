//
//  BreweryHelper.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

extension Brewery {
    
    static func shouldBeOpened(for description: String?) -> Bool {
        guard let description = description else { return true }
        for string in Constants.closedBreweryLabels {
            if description.contains(string) {
                return false
            }
        }
        return true
    }
    
    static func splitName(_ name: String) -> (name: String, description: String?, city: String?)? {
        
        let nameDescriptionCityGroups = name.regexMatches(RegexPattern.nameDescriptionCityPattern)
        if nameDescriptionCityGroups.count > 0 && nameDescriptionCityGroups[0].count > 0 {
            let matches = nameDescriptionCityGroups[0]
            return (matches[1], matches[2], matches[3])
        }
        
        let nameDescriptionGroups = name.regexMatches(RegexPattern.nameDescriptionPattern)
        if nameDescriptionGroups.count > 0 && nameDescriptionGroups[0].count > 0 {
            let matches = nameDescriptionGroups[0]
            return (matches[1], matches[2], nil)
        }
        
        let nameCityGroups = name.regexMatches(RegexPattern.nameCityPattern)
        if nameCityGroups.count > 0 && nameCityGroups[0].count > 0 {
            let matches = nameCityGroups[0]
            let splitted = matches[1].split(separator: "-")
            if splitted.count == 2 {
                return (String(splitted[0]), String(splitted[1]), matches[2])
            } else {
                return (matches[1], nil, matches[2])
            }
        }
        
        return nil
    }
    
}
