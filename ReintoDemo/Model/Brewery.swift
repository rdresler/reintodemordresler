//
//  Brewery.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct Brewery: Codable {
    let id: Int
    let name, url: String
    var city, description: String?
    let opened: Bool
}

extension Brewery {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        url = try values.decode(String.self, forKey: .url)
        let tempName = try values.decode(String.self, forKey: .name)
        
        if let (name, description, city) = Brewery.splitName(tempName) {
            
            self.name = name
            self.description = description
            
            if var description = description {
                
                let matches = description.regexMatches(RegexPattern.dotPattern)
                if matches.count > 0 && matches[0].count >= 2 {
                    description = matches[0][1]
                }
                self.description = description
                .replacingOccurrences(of: ", ", with: "\n")
                .replacingOccurrences(of: " a ", with: "\n")
                .removedStrings(Constants.breweryLabelsToRemove)
                .trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            self.city = city
        } else {
            name = tempName
            city = try? values.decode(String.self, forKey: .city)
            description = try? values.decode(String.self, forKey: .description)
        }
        
        if let opened = try? values.decode(Bool.self, forKey: .opened) {
            self.opened = opened
        } else {
            self.opened = Brewery.shouldBeOpened(for: description)
        }
        
    }
    
}
