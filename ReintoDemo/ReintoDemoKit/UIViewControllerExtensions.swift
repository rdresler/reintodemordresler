//
//  UIViewControllerExtensions.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String? = nil, actions: [UIAlertAction] = [], dismiss: Bool = true, dismissAfter: Double = 2) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { alert.addAction($0) }
        
        present(alert, animated: true)
        
        if dismiss {
            Timer.scheduledTimer(withTimeInterval: dismissAfter, repeats: false) { _ in
                alert.dismiss(animated: true)
            }
        }
    }
    
}
