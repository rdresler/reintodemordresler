//
//  BreweriesProvider.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

class BreweriesProvider {
    
    private var repository = BreweriesRepository()
    
    private var fetching: Bool = false
    
    func fetch(_ get: Bool, completion: @escaping ([Brewery]?) -> Void) {
        if get {
            if repository.info.offset > 0 {
                return completion(repository.breweries)
            } else {
                self.getBreweries(completion: completion)
            }
        } else {
            if fetching {
                return
            } else {
                if repository.info.reachedEndOfItems {
                    return
                } else {
                    self.getBreweries(completion: completion)
                }
            }
        }
    }
    
    private func getBreweries(completion: @escaping ([Brewery]?) -> Void) {
        
        fetching = true
        
        guard let url = URL(string: APIURL.breweries + "userId=\(UserRepository.shared.userId)&skip=\(repository.info.offset)") else {
            fatalError("Invalid URL: \(APIURL.breweries)")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "get"
        
        URLSession(configuration: .default).dataTask(with: request) { data, response, error in
            
            if let error = error { print(error) }
            
            guard let data = data else {
                print("Error: did not receive data")
                DispatchQueue.main.async {
                    self.fetching = false
                    completion(nil)
                }
                return
            }
            do {
                let breweries = try JSONDecoder().decode([Brewery].self, from: data)
                self.repository.save(breweries)
                
                DispatchQueue.main.async {
                    self.fetching = false
                    completion(self.repository.breweries)
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    self.fetching = false
                    completion(nil)
                }
            }
        }.resume()
        
    }
    
}
