//
//  UserRepository.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 26/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

class UserRepository {
    
    private let key = "userId"
    
    private init() {}
    
    var userExists: Bool {
        return UserDefaults.standard.object(forKey: key) as? Int != nil
    }
    
    var userId: Int {
        get {
            return UserDefaults.standard.integer(forKey: key)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
    
    static let shared = UserRepository()
    
}
