//
//  Constants.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 26/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct Constants {
    static let closedBreweryLabels = ["V přípravě", "Připravuje se otevření"]
    static let breweryLabelsToRemove = ["Piva", "Pivo", "fotografie", "video", "atd", ","]
}
