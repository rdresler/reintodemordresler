//
//  BreweryPreviewViewController.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import WebKit

class BreweryPreviewViewController: UIViewController {
    
    static func instantiate(breweryUrl: URL) -> BreweryPreviewViewController {
        let controller = BreweryPreviewViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.webView.load(URLRequest(url: breweryUrl))
        return controller
    }
    
    let webView = WKWebView()
    
    var blurView: UIView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        return UIVisualEffectView(effect: blurEffect)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        setBlur()
        setWebView()
    }
    
    private func setBlur() {
        view.addSubview(blurView)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        blurView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        blurView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        blurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognized(_:))))
    }
    
    private func setWebView() {
        view.addSubview(webView)
        webView.layer.masksToBounds = true
        webView.layer.cornerRadius = 25
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 25).isActive = true
        webView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -25).isActive = true
        webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
    }
    
    @objc func tapGestureRecognized(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true)
    }
    
}

extension BreweryPreviewViewController: WKNavigationDelegate {
}
