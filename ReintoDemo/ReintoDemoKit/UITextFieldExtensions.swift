//
//  UITextFieldExtensions.swift
//  ReintoDemo
//
//  Created by Robert Dresler on 25/03/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setPaddingTo(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: frame.size.height))
        leftView = paddingView
        leftViewMode = .always
        rightView = paddingView
        rightViewMode = .always
    }
    
}
